import os
import sys
import openpyxl
import Keirin
import pandas as pd
import datetime
import sqlite3

if __name__ == '__main__':
    db_name = 'scraping.db'
    conn = sqlite3.connect(db_name)
    cur = conn.cursor()

    now = datetime.datetime.now()
    d = now.strftime('%y%m%d_%H%M')
    ymd = now.strftime('%Y/%m/%d')

    dpath = os.path.dirname(sys.argv[0])
    rpath = os.path.join(dpath, 'results')
    ppath = os.path.join(rpath, 'レース情報（全件）' + d + '.csv')

    f = open(ppath, 'w', newline='', encoding='utf_8_sig')
    f.write(','.join(['レースURL', '開催日', '開催地', '開催賞・杯名', 'Round', '性別', '車立', '選手名', '期別情報', '所属地域',
                      '年齢', '車番', '枠番', '級班', '脚質', 'ギア倍数', '競走得点', '並び予想', '着順', '着順一覧',
                      '発売票数（3連単）', '発売票数（2車単）', '発売票数（3連複）', '発売票数（2車複）', '発売票数（2枠単）',
                      '発売票数（2枠複）', '発売票数（ワイド）', '払戻し金（3連単）', '人気（3連単）', '払戻し金（2車単）',
                      '人気（2車単）', '払戻し金（3連複）', '人気（3連複）', '払戻し金（2車複）', '人気（2車複）',
                      '払戻し金（2枠単）', '人気（2枠単）', '払戻し金（2枠複）', '人気（2枠複）', '払戻し金（ワイド１）', '人気（ワイド１）',
                      '払戻し金（ワイド２）', '人気（ワイド２）', '払戻し金（ワイド３）', '人気（ワイド３）']) + '\n')
    for row in cur.execute('SELECT url, race_date, venue, race_title, round, sex, frames, racer_name'
                           ', racer_period, home_town, age, bicycle_num, frame_num, rank, race_type, gear, point'
                           ', pred_line, racer_rank, rets_str, odds_rentan_3, odds_shatan_2, odds_renhuku_3'
                           ', odds_shahuku_2, odds_wakutan_2, odds_wakufuku_2, odds_wide, refund_rentan_3'
                           ', pred_rank_rentan_3, refund_shatan_2, pred_rank_shatan_2, refund_renhuku_3'
                           ', pred_rank_renhuku_3, refund_shahuku_2, pred_rank_shahuku_2, refund_wakutan_2'
                           ', pred_rank_wakutan_2, refund_wakufuku_2, pred_rank_wakufuku_2, refund_wide_1'
                           ', pred_rank_wide_1, refund_wide_2, pred_rank_wide_2, refund_wide_3, pred_rank_wide_3 '
                           ' FROM race_history '):
        db_row = ','.join(['"' + str(c).replace(',', '') + '"' for c in row])
        f.write(db_row + '\n')

    f.close()
