import os
import sys
import time
import datetime

import openpyxl
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.select import Select
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from bs4 import BeautifulSoup
import pandas as pd
import re
import sqlite3


# dict_factoryの定義
def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


class Keirin:
    base_url = 'https://keirin.kdreams.jp/racecard/'
    current_dir = os.path.dirname(sys.argv[0])
    db_name = 'scraping.db'

    def __init__(self, conditions):
        self.begin_date = datetime.datetime.strptime(str(conditions['検索開始日'][0]), '%Y%m%d')
        self.end_date = datetime.datetime.strptime(str(conditions['検索終了日'][0]), '%Y%m%d')
        #        self.rets = pd.DataFrame(index=[], columns=self.columns)
        # ブラウザを立ち上げない
        options = Options()
        options.add_argument('--headless')
        options.add_argument("--no-sandbox")
        options.add_argument('--disable-logging')
        options.add_argument('--log-level=3')
        options.add_experimental_option('excludeSwitches', ['enable-logging'])

        # 自身の環境にインストールされているGoogle Chromeのバージョンに併せてドライバーをインストール
        self.driver = webdriver.Chrome(ChromeDriverManager().install(), options=options)
        self.init_db()
        self.race_history = self.select_race_history()

    def init_db(self):
        conn = sqlite3.connect(self.db_name)
        conn.row_factory = dict_factory
        cur = conn.cursor()
        cur.execute(
            'CREATE TABLE if not exists race_history(id INTEGER PRIMARY KEY AUTOINCREMENT, url STRING, race_date STRING'
            ', venue STRING, race_title STRING, race_status STRING, round STRING, sex STRING, frames STRING'
            ', racer_name STRING, racer_period INTEGER, home_town STRING, age INTEGER, bicycle_num INTEGER'
            ', frame_num INTEGER, rank STRING, race_type STRING, gear REAL, point REAL, pred_line STRING'
            ', racer_rank INTEGER, rets_str STRING, odds_rentan_3 INTEGER, odds_shatan_2 INTEGER'
            ', odds_renhuku_3 INTEGER, odds_shahuku_2 INTEGER, odds_wakutan_2 INTEGER, odds_wakufuku_2 INTEGER'
            ', odds_wide INTEGER, refund_rentan_3 INTEGER, pred_rank_rentan_3 INTEGER, refund_shatan_2 INTEGER'
            ', pred_rank_shatan_2 INTEGER, refund_renhuku_3 INTEGER, pred_rank_renhuku_3 INTEGER'
            ', refund_shahuku_2 INTEGER, pred_rank_shahuku_2 INTEGER, refund_wakutan_2 INTEGER'
            ', pred_rank_wakutan_2 INTEGER, refund_wakufuku_2 INTEGER, pred_rank_wakufuku_2 INTEGER'
            ', refund_wide_1 INTEGER, pred_rank_wide_1 INTEGER, refund_wide_2 INTEGER, pred_rank_wide_2 INTEGER'
            ', refund_wide_3 INTEGER, pred_rank_wide_3 INTEGER)'
        )
        cur.execute(
            'CREATE TABLE if not exists race_odds_history(id INTEGER PRIMARY KEY AUTOINCREMENT, url STRING, '
            'odds_rank_rentan_3_line STRING, odds_rank_rentan_3_odds STRING, odds_rank_shatan_2_line STRING, '
            'odds_rank_shatan_2_odds STRING, odds_rank_renhuku_3_line STRING, odds_rank_renhuku_3_odds STRING, '
            'odds_rank_shahuku_2_line STRING, odds_rank_shahuku_2_odds STRING, odds_rank_wide_line STRING, '
            'odds_rank_wide_odds STRING, odds_table_rentan_3 BLOB, odds_table_shatan_2 BLOB, odds_table_renhuku_3 BLOB,'
            'odds_table_shahuku_2 BLOB, odds_table_wide BLOB)'
        )
        conn.commit()
        conn.close()

    def select_race_history(self):
        conn = sqlite3.connect(self.db_name)
        #        conn.row_factory = dict_factory
        cur = conn.cursor()
        cur.execute("select DISTINCT url from race_history")
        return cur.fetchall()

    def search_schedules(self):
        search_date = self.begin_date
        while search_date <= self.end_date:
            self.search_races(search_date)
            search_date = search_date + datetime.timedelta(days=1)

        self.driver.close()
        self.driver.quit()

    def search_races(self, search_date):
        self.driver.get(self.base_url + search_date.strftime('%Y/%m/%d'))
        time.sleep(1)
        print(search_date.strftime('%Y/%m/%d') + ' レース情報取得中')

        html = self.driver.page_source.encode('utf-8')
        soup = BeautifulSoup(html, 'html.parser')

        table = soup.find('div', class_='raceinfo_table')
        if table is None:
            return
        races = table.findAll('a', href=re.compile('racedetail'))
        race_detail_links = []
        for race in races:
            if (race['href'],) not in self.race_history:
                race_detail_links.append({'url': race['href'], 'round': race.text})

        for race_detail_link in race_detail_links:
            self.search_race(race_detail_link, search_date)

    def search_race(self, race_detail_link, search_date):
        self.driver.get(race_detail_link['url'])
        time.sleep(1)

        html = self.driver.page_source.encode('utf-8')
        soup = BeautifulSoup(html, 'html.parser')

        # DBを作成する（既に作成されていたらこのDBに接続する）
        conn = sqlite3.connect(self.db_name)

        try:
            # 開催地
            venue = self.getElementValue(soup.find('h1', class_='raceinfo_headline')).replace(' レース詳細', '')
            # 開催賞・杯名
            race_title = self.getElementValue(soup.find('p', class_='raceinfo_contents-title')).replace("\n", "")
            # 勝ち上がり賞
            race_status = self.getElementValue(soup.find('div', class_='racecard_header').find('span', class_='status'))
            # 男女
            racecard_header = soup.find('div', class_='racecard_header')
            race_category = self.getElementValue(racecard_header.find('span', class_='status'))
            sex = '女性' if 'ガールズ' in race_category else '男性'
            # 並び予想
            line_position = soup.find('div', class_='line_position')
            icons = line_position.findAll('span', class_='icon_p')
            pred_line = ''
            for icon in icons:
                icon_class = icon['class']
                if 1 < len(icon_class):
                    if icon_class[1] == 'space':
                        pred_line += ' '
                    else:
                        icon_txt = icon.text
                        if icon_txt in ["(", ")"]:
                            pred_line += icon_txt
                else:
                    icon_txt = icon.find('span').text
                    if icon_txt == '←':
                        continue
                    else:
                        pred_line += icon_txt
            pred_line = pred_line.strip()
            # 該当レースの結果

            # region 各賭式の投票数
            # 3連単
            odds_status = soup.find('div', id='JS_ODDSSTATUS_3rentan')
            odds_rentan_3 = self.getElementValue(odds_status.find('dd')).replace(',', '').replace('円', '') \
                if odds_status is not None else None
            # 2車単
            odds_status = soup.find('div', id='JS_ODDSSTATUS_2shatan')
            odds_shatan_2 = self.getElementValue(odds_status.find('dd')).replace(',', '').replace('円', '') \
                if odds_status is not None else None
            # 3連複
            odds_status = soup.find('div', id='JS_ODDSSTATUS_3renhuku')
            odds_renhuku_3 = self.getElementValue(odds_status.find('dd')).replace(',', '').replace('円', '') \
                if odds_status is not None else None
            # 2車複
            odds_status = soup.find('div', id='JS_ODDSSTATUS_2shahuku')
            odds_shahuku_2 = self.getElementValue(odds_status.find('dd')).replace(',', '').replace('円', '') \
                if odds_status is not None else None
            # 2枠単
            odds_status = soup.find('div', id='JS_ODDSNAV_2wakutan')
            odds_wakutan_2 = self.getElementValue(odds_status.find('dd')).replace(',', '').replace('円', '') \
                if odds_status is not None else None
            # 2枠複
            odds_status = soup.find('div', id='JS_ODDSNAV_2wakuhuku')
            odds_wakufuku_2 = self.getElementValue(odds_status.find('dd')).replace(',', '').replace('円', '') \
                if odds_status is not None else None
            # ワイド
            odds_status = soup.find('div', id='JS_ODDSSTATUS_wide')
            odds_wide = self.getElementValue(odds_status.find('dd')).replace(',', '').replace('円', '') \
                if odds_status is not None else None
            # endregion

            # region 結果
            ret_tbl = soup.find('table', class_='result_table')
            trs = ret_tbl.findAll('tr')
            rets = {}
            rets_str = ''
            first = True
            for tr in trs:
                if first:
                    first = False
                    continue
                rider = self.getElementValue(tr.find('td', class_='rider'))
                rank = self.getElementValue(tr.findAll('td')[1])
                frame_num = self.getElementValue(tr.findAll('td')[2])
                rets[rider] = rank
                rets_str = rets_str + '-' + frame_num if rets_str != '' else frame_num
            # endregion

            # region 各賭け式の払い戻し金・各賭け式の的中した目が何番人気だったか
            refund_tbl = soup.find('table', class_='refund_table')
            dls = refund_tbl.findAll('dl', class_='cf')
            # 3連単
            refund_status = self.getElementValue(dls[8].find('dt'))
            refund_rentan_3 = dls[8].find('dd').contents[0].replace(',', '').replace('円', '') \
                if refund_status != '未発売' else None
            pred_rank_rentan_3 = self.getElementValue(dls[8].find('dd').contents[1]).replace('(', '').replace(')', '') \
                if refund_status != '未発売' else ''
            # 2車単
            refund_status = self.getElementValue(dls[7].find('dt'))
            refund_shatan_2 = dls[7].find('dd').contents[0].replace(',', '').replace('円', '') \
                if refund_status != '未発売' else None
            pred_rank_shatan_2 = self.getElementValue(dls[7].find('dd').contents[1]).replace('(', '').replace(')', '') \
                if refund_status != '未発売' else ''
            # 3連複
            refund_status = self.getElementValue(dls[2].find('dt'))
            refund_renhuku_3 = dls[2].find('dd').contents[0].replace(',', '').replace('円', '') \
                if refund_status != '未発売' else None
            pred_rank_renhuku_3 = self.getElementValue(dls[2].find('dd').contents[1]).replace('(', '').replace(')', '') \
                if refund_status != '未発売' else ''
            # 2車複
            refund_status = self.getElementValue(dls[1].find('dt'))
            refund_shahuku_2 = dls[1].find('dd').contents[0].replace(',', '').replace('円', '') \
                if refund_status != '未発売' else None
            pred_rank_shahuku_2 = self.getElementValue(dls[1].find('dd').contents[1]).replace('(', '').replace(')', '') \
                if refund_status != '未発売' else ''
            # 2枠単
            refund_status = self.getElementValue(dls[6].find('dt'))
            refund_wakutan_2 = dls[6].find('dd').contents[0].replace(',', '').replace('円', '') \
                if refund_status != '未発売' else None
            pred_rank_wakutan_2 = self.getElementValue(dls[6].find('dd').contents[1]).replace('(', '').replace(')', '') \
                if refund_status != '未発売' else ''
            # 2枠複
            refund_status = self.getElementValue(dls[0].find('dt'))
            refund_wakufuku_2 = dls[0].find('dd').contents[0].replace(',', '').replace('円', '') \
                if refund_status != '未発売' else None
            pred_rank_wakufuku_2 = self.getElementValue(dls[0].find('dd').contents[1]).replace('(', '').replace(')', '') \
                if refund_status != '未発売' else ''
            # ワイド
            refund_status = self.getElementValue(dls[3].find('dt'))
            refund_wide_1 = dls[3].find('dd').contents[0].replace(',', '').replace('円', '') \
                if refund_status != '未発売' else None
            pred_rank_wide_1 = self.getElementValue(dls[3].find('dd').contents[1]).replace('(', '').replace(')', '') \
                if refund_status != '未発売' else ''

            refund_status = self.getElementValue(dls[4].find('dt'))
            refund_wide_2 = dls[4].find('dd').contents[0].replace(',', '').replace('円', '') \
                if refund_status != '未発売' else None
            pred_rank_wide_2 = self.getElementValue(dls[4].find('dd').contents[1]).replace('(', '').replace(')', '') \
                if refund_status != '未発売' else ''

            refund_status = self.getElementValue(dls[5].find('dt'))
            refund_wide_3 = dls[5].find('dd').contents[0].replace(',', '').replace('円', '') \
                if refund_status != '未発売' else None
            pred_rank_wide_3 = self.getElementValue(dls[5].find('dd').contents[1]).replace('(', '').replace(')', '') \
                if refund_status != '未発売' else ''
            # endregion

            racer_tbl = soup.find('table', class_='racecard_table')
            racer_rows = racer_tbl.findAll('tr', class_=re.compile('^n'))
            # 枠数
            frames = len(racer_rows)
            row_span = 1
            bef_frame_num = None
            for racer in racer_rows:
                # 選手名
                racer_name = self.getElementValue(racer.find('td', class_='rider').contents[0])

                racer_data = self.getElementValue(racer.find('span', class_='home')).split('/')
                # 期別情報
                racer_period = racer_data[2]
                # その選手の所属地域
                home_town = racer_data[0]
                # 年齢
                age = racer_data[1]
                # 車番
                bicycle_num = self.getElementValue(racer.find('td', class_='num'))

                idx_slide = 0
                if 1 < row_span:
                    idx_slide = 1
                bracket = racer.find('td', class_='bracket')
                if bracket is None:
                    row_span = row_span - 1
                    # 枠番
                    frame_num = bef_frame_num
                else:
                    if bracket.has_key('rowspan'):
                        row_span = int(bracket['rowspan'])
                        bef_frame_num = self.getElementValue(bracket)
                    # 枠番
                    frame_num = self.getElementValue(bracket)

                tbl_cells = racer.findAll('td')
                # 級班
                rank = self.getElementValue(tbl_cells[6 - idx_slide])
                # 脚質
                race_type = self.getElementValue(tbl_cells[7 - idx_slide])
                # ギア倍数
                gear_cell = tbl_cells[8 - idx_slide]
                gear_after = self.getElementValue(gear_cell.find('span', class_='after'))
                gear = self.getElementValue(gear_cell) if gear_after == '' else gear_after
                # 競争得点
                point = self.getElementValue(tbl_cells[9 - idx_slide])

                # 着順
                racer_rank = rets[racer_name] if racer_name in rets else ''

                # SQLiteを操作するためのカーソルを作成
                cur = conn.cursor()
                cur.execute(
                    'INSERT INTO race_history(url, race_date, venue, race_title, race_status, round, sex, frames, racer_name'
                    ', racer_period, home_town, age, bicycle_num, frame_num, rank, race_type, gear, point, pred_line'
                    ', racer_rank, rets_str, odds_rentan_3, odds_shatan_2, odds_renhuku_3, odds_shahuku_2, odds_wakutan_2'
                    ', odds_wakufuku_2, odds_wide, refund_rentan_3, pred_rank_rentan_3, refund_shatan_2, pred_rank_shatan_2'
                    ', refund_renhuku_3, pred_rank_renhuku_3, refund_shahuku_2, pred_rank_shahuku_2, refund_wakutan_2'
                    ', pred_rank_wakutan_2, refund_wakufuku_2, pred_rank_wakufuku_2, refund_wide_1, pred_rank_wide_1'
                    ', refund_wide_2, pred_rank_wide_2, refund_wide_3, pred_rank_wide_3)'
                    'VALUES(:url, :race_date, :venue, :race_title, :race_status, :round, :sex,'
                    ':frames, :racer_name, :racer_period, :home_town, :age, :bicycle_num, '
                    ':frame_num, :rank, :race_type, :gear, :point, :pred_line, :racer_rank, :rets_str, :odds_rentan_3, '
                    ':odds_shatan_2, :odds_renhuku_3, :odds_shahuku_2, :odds_wakutan_2, :odds_wakufuku_2, :odds_wide, '
                    ':refund_rentan_3, :pred_rank_rentan_3, :refund_shatan_2, :pred_rank_shatan_2, :refund_renhuku_3, '
                    ':pred_rank_renhuku_3, :refund_shahuku_2, :pred_rank_shahuku_2, :refund_wakutan_2, '
                    ':pred_rank_wakutan_2, :refund_wakufuku_2, :pred_rank_wakufuku_2, :refund_wide_1, :pred_rank_wide_1, '
                    ':refund_wide_2, :pred_rank_wide_2, :refund_wide_3, :pred_rank_wide_3)'
                    , (race_detail_link['url'], search_date.strftime('%Y%m%d'), venue, race_title, race_status,
                       race_detail_link['round']
                       , sex, frames, racer_name, racer_period, home_town, age, bicycle_num, frame_num, rank, race_type
                       , gear, point, pred_line, racer_rank, rets_str, odds_rentan_3, odds_shatan_2, odds_renhuku_3
                       , odds_shahuku_2, odds_wakutan_2, odds_wakufuku_2, odds_wide, refund_rentan_3, pred_rank_rentan_3
                       , refund_shatan_2, pred_rank_shatan_2, refund_renhuku_3, pred_rank_renhuku_3, refund_shahuku_2
                       , pred_rank_shahuku_2, refund_wakutan_2, pred_rank_wakutan_2, refund_wakufuku_2,
                       pred_rank_wakufuku_2
                       , refund_wide_1, pred_rank_wide_1, refund_wide_2, pred_rank_wide_2, refund_wide_3,
                       pred_rank_wide_3)
                )
                conn.commit()
                cur.close()

            # オッズ順位
            odds_table_rentan_3 = []
            odds_rank_rentan_3_area = soup.find('div', id='JS_ODDSCONTENTS_3rentan')
            odds_rank_rentan_3_tbl = odds_rank_rentan_3_area.find('div', class_='oddspop_table_wrapper')
            odds_rank_rentan_3_line = self.getElementValue(odds_rank_rentan_3_tbl.find('span', class_='num'))
            odds_rank_rentan_3_odds = self.getElementValue(odds_rank_rentan_3_tbl.find('span', class_='odds'))

            odds_rank_rentan_3_ind_tbls = odds_rank_rentan_3_area.findAll('table', 'odds_table')
            odds_rank_rentan_3_nums = odds_rank_rentan_3_area.findAll('a')
            odds_rank_rentan_3_nums_lt = []
            for odds_rank_rentan_3_num in odds_rank_rentan_3_nums:
                odds_rank_rentan_3_nums_lt.append(self.getElementValue(odds_rank_rentan_3_num))

            for top_idx, odds_rank_rentan_3_ind_tbl in enumerate(odds_rank_rentan_3_ind_tbls):
                top_num = odds_rank_rentan_3_nums_lt[top_idx]
                trs = odds_rank_rentan_3_ind_tbl.findAll('tr')
                for tr_idx, tr in enumerate(trs):
                    if tr_idx < 3:
                        continue
                    tds = tr.findAll('td')
                    for td_idx, td in enumerate(tds):
                        if td.has_attr('class'):
                            continue

                        odds = self.getElementValue(td)
                        odds_table_rentan_3.append(str(top_num) + '-' +
                                          (str(td_idx + 1) if (td_idx + 1) < int(top_num) else str(td_idx + 2)) + '-' +
                                          (str(tr_idx - 2) if (tr_idx - 2) < int(top_num) else str(tr_idx - 1)))
                        odds_table_rentan_3.append(str(odds))

            odds_table_shatan_2 = []
            odds_rank_shatan_2_area = soup.find('div', id='JS_ODDSCONTENTS_2shatan')
            odds_rank_shatan_2_tbl = odds_rank_shatan_2_area.find('div', class_='oddspop_table_wrapper')
            odds_rank_shatan_2_line = self.getElementValue(odds_rank_shatan_2_tbl.find('span', class_='num'))
            odds_rank_shatan_2_odds = self.getElementValue(odds_rank_shatan_2_tbl.find('span', class_='odds'))

            odds_rank_shatan_2_ind_tbl = odds_rank_shatan_2_area.find('table', 'odds_table')
            trs = odds_rank_shatan_2_ind_tbl.findAll('tr')
            for tr_idx, tr in enumerate(trs):
                if tr_idx < 2:
                    continue
                tds = tr.findAll('td')
                for td_idx, td in enumerate(tds):
                    if td.has_attr('class'):
                        continue

                    odds = self.getElementValue(td)
                    odds_table_shatan_2.append(str(td_idx + 1) + '-' + str(tr_idx - 1))
                    odds_table_shatan_2.append(str(odds))

            odds_table_renhuku_3 = []
            odds_rank_renhuku_3_area = soup.find('div', id='JS_ODDSCONTENTS_3renhuku')
            odds_rank_renhuku_3_tbl = odds_rank_renhuku_3_area.find('div', class_='oddspop_table_wrapper')
            odds_rank_renhuku_3_line = self.getElementValue(odds_rank_renhuku_3_tbl.find('span', class_='num'))
            odds_rank_renhuku_3_odds = self.getElementValue(odds_rank_renhuku_3_tbl.find('span', class_='odds'))

            odds_rank_renhuku_3_ind_tbls = odds_rank_renhuku_3_area.findAll('table', 'odds_table')
            for odds_rank_renhuku_3_ind_tbl in odds_rank_renhuku_3_ind_tbls:
                trs = odds_rank_renhuku_3_ind_tbl.findAll('tr')
                tbl_frame = 0
                tr_frame = 0
                for tr_idx, tr in enumerate(trs):
                    if tr_idx == 0:
                        tbl_frame = self.getElementValue(tr)
                    if tr_idx < 2:
                        continue

                    tds = tr.findAll(['td', 'th'])
                    td_frame = 0
                    for td_idx, td in enumerate(tds):
                        if td.has_attr('rowspan'):
                            tr_frame = self.getElementValue(td)
                            continue
                        if td.has_attr('class'):
                            td_frame = self.getElementValue(td)
                            continue

                        odds = self.getElementValue(td)
                        odds_table_renhuku_3.append(str(tbl_frame) + '=' + str(tr_frame) + '=' + str(td_frame))
                        odds_table_renhuku_3.append(str(odds))

            odds_table_shahuku_2 = []
            odds_rank_shahuku_2_area = soup.find('div', id='JS_ODDSCONTENTS_2shahuku')
            odds_rank_shahuku_2_tbl = odds_rank_shahuku_2_area.find('div', class_='oddspop_table_wrapper')
            odds_rank_shahuku_2_line = self.getElementValue(odds_rank_shahuku_2_tbl.find('span', class_='num'))
            odds_rank_shahuku_2_odds = self.getElementValue(odds_rank_shahuku_2_tbl.find('span', class_='odds'))

            odds_rank_shahuku_2_ind_tbl = odds_rank_shahuku_2_area.find('table', 'odds_table')
            trs = odds_rank_shahuku_2_ind_tbl.findAll('tr')
            for tr_idx, tr in enumerate(trs):
                if tr_idx < 2:
                    continue
                tds = tr.findAll('td')
                for td_idx, td in enumerate(tds):
                    if td.has_attr('class'):
                        continue

                    odds = self.getElementValue(td)
                    odds_table_shahuku_2.append(str(td_idx + 1) + '=' + str(tr_idx))
                    odds_table_shahuku_2.append(str(odds))

            odds_table_wide = []
            odds_rank_wide_area = soup.find('div', id='JS_ODDSCONTENTS_wide')
            odds_rank_wide_tbl = odds_rank_wide_area.find('div', class_='oddspop_table_wrapper')
            odds_rank_wide_line = self.getElementValue(odds_rank_wide_tbl.find('span', class_='num'))
            odds_rank_wide_odds = self.getElementValue(odds_rank_wide_tbl.find('span', class_='odds'))

            odds_rank_wide_ind_tbl = odds_rank_wide_area.find('table', 'odds_table')
            trs = odds_rank_wide_ind_tbl.findAll('tr')
            for tr_idx, tr in enumerate(trs):
                if tr_idx < 2:
                    continue
                tds = tr.findAll('td')
                for td_idx, td in enumerate(tds):
                    if td.has_attr('class'):
                        continue

                    odds = self.getElementValue(td)
                    odds_table_wide.append(str(td_idx + 1) + '=' + str(tr_idx - 1))
                    odds_table_wide.append(str(odds))

            cur = conn.cursor()
            cur.execute(
                'INSERT INTO race_odds_history(url, odds_rank_rentan_3_line, odds_rank_rentan_3_odds, '
                'odds_rank_shatan_2_line, odds_rank_shatan_2_odds, odds_rank_renhuku_3_line, '
                'odds_rank_renhuku_3_odds, odds_rank_shahuku_2_line, odds_rank_shahuku_2_odds, '
                'odds_rank_wide_line, odds_rank_wide_odds, odds_table_rentan_3, odds_table_shatan_2, '
                'odds_table_renhuku_3, odds_table_shahuku_2, odds_table_wide)'
                'VALUES(:url, :odds_rank_rentan_3_line, :odds_rank_rentan_3_odds, '
                ':odds_rank_shatan_2_line, :odds_rank_shatan_2_odds, :odds_rank_renhuku_3_line, '
                ':odds_rank_renhuku_3_odds, :odds_rank_shahuku_2_line, :odds_rank_shahuku_2_odds, '
                ':odds_rank_wide_line, :odds_rank_wide_odds, :odds_table_rentan_3, :odds_table_shatan_2, '
                ':odds_table_renhuku_3, :odds_table_shahuku_2, :odds_table_wide)'
                , (race_detail_link['url'], odds_rank_rentan_3_line, odds_rank_rentan_3_odds,
                   odds_rank_shatan_2_line, odds_rank_shatan_2_odds, odds_rank_renhuku_3_line,
                   odds_rank_renhuku_3_odds, odds_rank_shahuku_2_line, odds_rank_shahuku_2_odds,
                   odds_rank_wide_line, odds_rank_wide_odds, ",".join(odds_table_rentan_3),
                   ",".join(odds_table_shatan_2), ",".join(odds_table_renhuku_3), ",".join(odds_table_shahuku_2),
                   ",".join(odds_table_wide))
            )
            conn.commit()
            cur.close()
        except:
            #            print(race_detail_link['url'] + 'は開催中止')
            print("")

        conn.close()

    @classmethod
    def getElementValue(cls, ele):
        return ele.text.strip() if ele is not None else ''
