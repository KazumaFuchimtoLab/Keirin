import os
import sys
import openpyxl
from Keirin import Keirin
import pandas as pd
import datetime


class Settings:
    def __init__(self):
        self.conditions = {}
        self.list_condition_keys = ['execute_sites']

        dpath = os.path.dirname(sys.argv[0])
        rpath = os.path.join(dpath, 'settings.xlsx')

        wb = openpyxl.load_workbook(rpath)
        sheet = wb['settings']
        for r in range(1, 100):
            key = sheet.cell(row=r, column=1).value
            if key is None:
                break
            values = []
            for c in range(2, 1000):
                value = sheet.cell(row=r, column=c).value
                if value is None:
                    break
                else:
                    values.append(value)

            self.conditions[key] = values


if __name__ == '__main__':
    settings = Settings()
    keirin = Keirin(settings.conditions)
    keirin.search_schedules()
