import os
import sys
import openpyxl
import Keirin
import pandas as pd
import datetime
import sqlite3


class Settings:
    def __init__(self):
        self.conditions = {}

        dpath = os.path.dirname(sys.argv[0])
        rpath = os.path.join(dpath, 'settings_filter.xlsx')

        wb = openpyxl.load_workbook(rpath)
        sheet = wb['settings']
        for r in range(2, 100):
            key = sheet.cell(row=r, column=1).value
            if key is None:
                break
            values = []
            for c in range(2, 1000):
                value = sheet.cell(row=r, column=c).value
                if value is None:
                    break
                else:
                    values.append(value)

            self.conditions[key] = values


def create_query_param_set_range_wide_rank(q, ps, column, val, opt):
    if opt == 0:
        q = q + ' and (pred_rank_wide_1 >= :' + column + ' or ' + \
            'pred_rank_wide_2 >= :' + column + ' or pred_rank_wide_3 >= :' + column + ')' \
            if 0 < len(q) else '(pred_rank_wide_1 >= :' + column + ' or ' + \
                               'pred_rank_wide_2 >= :' + column + ' or pred_rank_wide_3 >= :' + column + ')'
    else:
        q = q + ' and (pred_rank_wide_1 <= :' + column + ' or ' + \
            'pred_rank_wide_2 <= :' + column + ' or pred_rank_wide_3 <= :' + column + ')' \
            if 0 < len(q) else '(pred_rank_wide_1 <= :' + column + ' or ' + \
                               'pred_rank_wide_2 <= :' + column + ' or pred_rank_wide_3 <= :' + column + ')'
    ps = ps + (val,)
    return q, ps


def create_query_param_set_range_wide_refund(q, ps, column, val, opt):
    if opt == 0:
        q = q + ' and (refund_wide_1 >= :' + column + ' or ' + \
            'refund_wide_2 >= :' + column + ' or refund_wide_3 >= :' + column + ')' \
            if 0 < len(q) else '(refund_wide_1 >= :' + column + ' or ' + \
                               'refund_wide_2 >= :' + column + ' or refund_wide_3 >= :' + column + ')'
    else:
        q = q + ' and (refund_wide_1 <= :' + column + ' or ' + \
            'refund_wide_2 <= :' + column + ' or refund_wide_3 <= :' + column + ')' \
            if 0 < len(q) else '(refund_wide_1 <= :' + column + ' or ' + \
                               'refund_wide_2 <= :' + column + ' or refund_wide_3 <= :' + column + ')'
    ps = ps + (val,)
    return q, ps


def create_query_param_set_range(q, ps, column, val, opt):
    if opt == 0:
        q = q + ' and ' + column + ' >= :' + column if 0 < len(q) else column + ' >= :' + column
    else:
        q = q + ' and ' + column + ' <= :' + column if 0 < len(q) else column + ' <= :' + column
    ps = ps + (val,)
    return q, ps


def create_query_param_set_match(q, ps, column, val, opt):
    q = q + ' and ' + column + ' = :' + column if 0 < len(q) else column + ' = :' + column
    ps = ps + (str(val),)
    return q, ps


def create_query_param_set_match_or(q, ps, column, val, opt):
    for idx, v in enumerate(val):
        if idx == 0 and 1 < len(val):
            q = q + ' and (' + column + ' = :' + column + '_' + str(idx) \
                if 0 < len(q) else ' (' + column + ' = :' + column + '_' + str(idx)
        elif idx == 0 and 1 == len(val):
            q = q + ' and ' + column + ' = :' + column + '_' + str(idx) \
                if 0 < len(q) else column + ' = :' + column + '_' + str(idx)
        elif idx == len(val) - 1:
            q = q + ' or ' + column + ' = :' + column + '_' + str(idx) + ')'
        else:
            q = q + ' or ' + column + ' = :' + column + '_' + str(idx) \
                if 0 < len(q) else ' or ' + column + ' = :' + column + '_' + str(idx)

        ps = ps + (str(v).strip(),)

    return q, ps


def create_query_param_set_home_town(q, ps, column, val, opt):
    q = q + " and replace(home_town, '　', '') = :" + column \
        if 0 < len(q) else "replace(home_town, '　', '') = :" + column
    ps = ps + (str(val),)
    return q, ps


def create_query_param_set_match_ym(q, ps, column, val, opt):
    q = q + ' and substr(' + column + ', 1, 6) = :' + column + '_ym' \
        if 0 < len(q) else 'substr(' + column + ', 1, 6) = :' + column + '_ym'
    ps = ps + (str(val),)
    return q, ps


def create_query_param_set_match_m(q, ps, column, val, opt):
    q = q + ' and substr(' + column + ', 5, 2) = :' + column + '_m' \
        if 0 < len(q) else 'substr(' + column + ', 5, 2) = :' + column + '_m'
    ps = ps + (str(val),)
    return q, ps


if __name__ == '__main__':
    db_name = 'scraping.db'
    conn = sqlite3.connect(db_name)
    cur = conn.cursor()

    now = datetime.datetime.now()
    d = now.strftime('%y%m%d_%H%M')
    ymd = now.strftime('%Y/%m/%d')

    dpath = os.path.dirname(sys.argv[0])
    rpath = os.path.join(dpath, 'results')
    ppath = os.path.join(rpath, 'レース情報（全件）' + d + '.csv')
    pppath = os.path.join(rpath, 'レース情報（簡易）' + d + '.csv')

    settings = Settings()
    query = ''
    params = ()
    # region 検索条件
    for key, values in settings.conditions.items():
        if values[2] == 0:
            continue
        if key == '選手名':
            query, params = create_query_param_set_match_or(query, params, 'racer_name', values[0].split(','),
                                                            values[1])
        elif key == '所属':
            query, params = create_query_param_set_home_town(query, params, 'home_town', values[0], values[1])
        elif key == '期':
            query, params = create_query_param_set_match(query, params, 'racer_period', values[0], values[1])
        elif key == '性別':
            query, params = create_query_param_set_match(query, params, 'sex', values[0], values[1])
        elif key == '開催地':
            query, params = create_query_param_set_match(query, params, 'venue', values[0], values[1])
        elif key == '開催月（年月）':
            query, params = create_query_param_set_match_ym(query, params, 'race_date', values[0], values[1])
        elif key == '開催月（月）':
            query, params = create_query_param_set_match_m(query, params, 'race_date', values[0], values[1])
        elif key == '発売票数（3連単）':
            query, params = create_query_param_set_range(query, params, 'odds_rentan_3', values[0], values[1])
        elif key == '発売票数（2車単）':
            query, params = create_query_param_set_range(query, params, 'odds_shatan_2', values[0], values[1])
        elif key == '発売票数（3連複）':
            query, params = create_query_param_set_range(query, params, 'odds_renhuku_3', values[0], values[1])
        elif key == '発売票数（2車複）':
            query, params = create_query_param_set_range(query, params, 'odds_shahuku_2', values[0], values[1])
        elif key == '発売票数（2枠単）':
            query, params = create_query_param_set_range(query, params, 'odds_wakutan_2', values[0], values[1])
        elif key == '発売票数（2枠複）':
            query, params = create_query_param_set_range(query, params, 'odds_wakufuku_2', values[0], values[1])
        elif key == '発売票数（ワイド）':
            query, params = create_query_param_set_range(query, params, 'odds_wide', values[0], values[1])
        elif key == '払戻し金（3連単）':
            query, params = create_query_param_set_range(query, params, 'refund_rentan_3', values[0], values[1])
        elif key == '払戻し金（2車単）':
            query, params = create_query_param_set_range(query, params, 'refund_shatan_2', values[0], values[1])
        elif key == '払戻し金（3連複）':
            query, params = create_query_param_set_range(query, params, 'refund_renhuku_3', values[0], values[1])
        elif key == '払戻し金（2車複）':
            query, params = create_query_param_set_range(query, params, 'refund_shahuku_2', values[0], values[1])
        elif key == '払戻し金（2枠単）':
            query, params = create_query_param_set_range(query, params, 'refund_wakutan_2', values[0], values[1])
        elif key == '払戻し金（2枠複）':
            query, params = create_query_param_set_range(query, params, 'refund_wakufuku_2', values[0], values[1])
        elif key == '払戻し金（ワイド）':
            query, params = create_query_param_set_range_wide_refund(query, params, 'refund_wide_1', values[0],
                                                                     values[1])
        elif key == '人気（3連単）':
            query, params = create_query_param_set_range(query, params, 'pred_rank_rentan_3', values[0], values[1])
        elif key == '人気（2車単）':
            query, params = create_query_param_set_range(query, params, 'pred_rank_shatan_2', values[0], values[1])
        elif key == '人気（3連複）':
            query, params = create_query_param_set_range(query, params, 'pred_rank_renhuku_3', values[0], values[1])
        elif key == '人気（2車複）':
            query, params = create_query_param_set_range(query, params, 'pred_rank_shahuku_2', values[0], values[1])
        elif key == '人気（2枠単）':
            query, params = create_query_param_set_range(query, params, 'pred_rank_wakutan_2', values[0], values[1])
        elif key == '人気（2枠複）':
            query, params = create_query_param_set_range(query, params, 'pred_rank_wakufuku_2', values[0], values[1])
        elif key == '人気（ワイド）':
            query, params = create_query_param_set_range_wide_rank(query, params, 'pred_rank_wide_1', values[0],
                                                                   values[1])
    # endregion

    sql = 'SELECT url, race_date, venue, race_title, race_status, round, sex, frames, racer_name' \
          ', racer_period, home_town, age, bicycle_num, frame_num, rank, race_type, gear, point' \
          ', pred_line, racer_rank, rets_str, odds_rentan_3, odds_shatan_2, odds_renhuku_3' \
          ', odds_shahuku_2, odds_wakutan_2, odds_wakufuku_2, odds_wide, refund_rentan_3' \
          ', pred_rank_rentan_3, refund_shatan_2, pred_rank_shatan_2, refund_renhuku_3' \
          ', pred_rank_renhuku_3, refund_shahuku_2, pred_rank_shahuku_2, refund_wakutan_2' \
          ', pred_rank_wakutan_2, refund_wakufuku_2, pred_rank_wakufuku_2, refund_wide_1' \
          ', pred_rank_wide_1, refund_wide_2, pred_rank_wide_2, refund_wide_3, pred_rank_wide_3 ' \
          ' FROM race_history '

    f = open(ppath, 'w', newline='', encoding='utf_8_sig')
    f.write(','.join(['レースURL', '開催日', '開催地', '開催賞・杯名', '勝ち上がり賞', 'Round', '性別', '車立', '選手名', '期別情報', '所属地域',
                      '年齢', '車番', '枠番', '級班', '脚質', 'ギア倍数', '競走得点', '並び予想', '着順', '着順一覧',
                      '発売票数（3連単）', '発売票数（2車単）', '発売票数（3連複）', '発売票数（2車複）', '発売票数（2枠単）',
                      '発売票数（2枠複）', '発売票数（ワイド）', '払戻し金（3連単）', '人気（3連単）', '払戻し金（2車単）',
                      '人気（2車単）', '払戻し金（3連複）', '人気（3連複）', '払戻し金（2車複）', '人気（2車複）',
                      '払戻し金（2枠単）', '人気（2枠単）', '払戻し金（2枠複）', '人気（2枠複）', '払戻し金（ワイド１）', '人気（ワイド１）',
                      '払戻し金（ワイド２）', '人気（ワイド２）', '払戻し金（ワイド３）', '人気（ワイド３）']) + '\n')
    if 0 < len(query):
        sql = sql + 'WHERE ' + query
        for row in cur.execute(sql, params):
            db_row = ','.join(['"' + str(c).replace(',', '') + '"' for c in row])
            f.write(db_row + '\n')
    else:
        for row in cur.execute(sql):
            db_row = ','.join(['"' + str(c).replace(',', '') + '"' for c in row])
            f.write(db_row + '\n')
    f.close()

    sql = 'select MAIN.url, MAIN.race_date, MAIN.venue, MAIN.race_title, MAIN.race_status, MAIN.round, MAIN.sex, MAIN.frames' \
          ', A.racer_name || A.racer_period, B.racer_name || B.racer_period, C.racer_name || C.racer_period' \
          ', D.racer_name || D.racer_period, E.racer_name || E.racer_period, F.racer_name || F.racer_period' \
          ', G.racer_name || G.racer_period, H.racer_name || H.racer_period, I.racer_name || I.racer_period' \
          ', MAIN.pred_line, MAIN.rets_str, MAIN.odds_rentan_3, MAIN.odds_shatan_2, MAIN.odds_renhuku_3' \
          ', MAIN.odds_shahuku_2, MAIN.odds_wakutan_2, MAIN.odds_wakufuku_2, MAIN.odds_wide, MAIN.refund_rentan_3' \
          ', MAIN.pred_rank_rentan_3, MAIN.refund_shatan_2, MAIN.pred_rank_shatan_2, MAIN.refund_renhuku_3' \
          ', MAIN.pred_rank_renhuku_3, MAIN.refund_shahuku_2, MAIN.pred_rank_shahuku_2, MAIN.refund_wakutan_2' \
          ', MAIN.pred_rank_wakutan_2, MAIN.refund_wakufuku_2, MAIN.pred_rank_wakufuku_2, MAIN.refund_wide_1' \
          ', MAIN.pred_rank_wide_1, MAIN.refund_wide_2, MAIN.pred_rank_wide_2, MAIN.refund_wide_3' \
          ', MAIN.pred_rank_wide_3, OD.odds_rank_rentan_3_line, OD.odds_rank_rentan_3_odds, OD.odds_rank_shatan_2_line' \
          ', OD.odds_rank_shatan_2_odds, OD.odds_rank_renhuku_3_line, OD.odds_rank_renhuku_3_odds' \
          ', OD.odds_rank_shahuku_2_line, OD.odds_rank_shahuku_2_odds, OD.odds_rank_wide_line' \
          ', OD.odds_rank_wide_odds '

    if settings.conditions['オッズ表出力'][1] == 1:
        sql = sql + ', OD.odds_table_rentan_3, OD.odds_table_shatan_2, OD.odds_table_renhuku_3, OD.odds_table_shahuku_2' \
                    ', OD.odds_table_wide '

    sql = sql + ' from (select * from race_history '
    sql2 = ' group by url ) as MAIN ' \
           'left join (select * from race_history where bicycle_num == 1) as A on MAIN.url == A.url ' \
           'left join (select * from race_history where bicycle_num == 2) as B on MAIN.url == B.url ' \
           'left join (select * from race_history where bicycle_num == 3) as C on MAIN.url == C.url ' \
           'left join (select * from race_history where bicycle_num == 4) as D on MAIN.url == D.url ' \
           'left join (select * from race_history where bicycle_num == 5) as E on MAIN.url == E.url ' \
           'left join (select * from race_history where bicycle_num == 6) as F on MAIN.url == F.url ' \
           'left join (select * from race_history where bicycle_num == 7) as G on MAIN.url == G.url ' \
           'left join (select * from race_history where bicycle_num == 8) as H on MAIN.url == H.url ' \
           'left join (select * from race_history where bicycle_num == 9) as I on MAIN.url == I.url ' \
           'left join (select * from race_odds_history) as OD on MAIN.url == OD.url'

    f = open(pppath, 'w', newline='', encoding='utf_8_sig')
    f.write(','.join(['レースURL', '’開催日', '開催地', '開催賞・杯名', '勝ち上がり賞', 'Round', '性別', '車立', '1番車'
                         , '2番車', '3番車', '4番車', '5番車', '6番車', '7番車', '8番車', '9番車', '並び予想'
                         , '着順一覧', '発売票数（3連単）', '発売票数（2車単）', '発売票数（3連複）', '発売票数（2車複）'
                         , '発売票数（2枠単）', '発売票数（2枠複）', '発売票数（ワイド）', '払戻し金（3連単）', '人気（3連単）'
                         , '払戻し金（2車単）', '人気（2車単）', '払戻し金（3連複）', '人気（3連複）', '払戻し金（2車複）'
                         , '人気（2車複）', '払戻し金（2枠単）', '人気（2枠単）', '払戻し金（2枠複）', '人気（2枠複）'
                         , '払戻し金（ワイド１）', '人気（ワイド１）', '払戻し金（ワイド２）', '人気（ワイド２）'
                         , '払戻し金（ワイド３）', '人気（ワイド３）', '3連単1番人気', '3連単1番人気配当'
                         , '3連複1番人気', '3連複1番人気配当', '2車単1番人気', '2車単1番人気配当'
                         , '2車複1番人気', '2車複1番人気配当', 'ワイド1番人気', 'ワイド1番人気配当']) + '\n')
    if 0 < len(query):
        sql = sql + 'WHERE ' + query + sql2
        for row in cur.execute(sql, params):
            data = []
            for r in row:
                values = str(r).split(',')
                for val in values:
                    data.append('"' + val + '"')

            db_row = ','.join(data)
            f.write(db_row + '\n')
    else:
        sql = sql + sql2
        for row in cur.execute(sql):
            data = []
            for r in row:
                values = str(r).split(',')
                for val in values:
                    data.append('"' + val + '"')

            db_row = ','.join(data)
            f.write(db_row + '\n')

    f.close()
